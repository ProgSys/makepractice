CC=gcc
CFLAGS=-I.
DEPS = point.h
OBJ = main.o point.o 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

makemain: $(OBJ)
	gcc -o $@ $^ $(CFLAGS) -lm

clean:
	rm -f $(OBJ)/*.o makemain *.gch
	