#include "point.h"
#include <math.h> 

float euclideanDistance (struct Point p1, struct Point p2){
	float res4x = p2.x - p1.x;
	float res4y = p2.y - p1.y;
	float res4z = p2.z - p2.z;
	float sum = pow(res4x,2) + pow(res4y,2) + pow(res4z,2);
	return sqrt(sum);
};


 
