#include <stdio.h>
#include "point.h"

int main()
{
	float x;
	struct Point p1, p2;
	printf("\nIngrese las coordenadas del primer punto:\n");
	printf("Coordenada x1: \t");
	scanf("%f",&x);
	p1.x = x;
	printf("Coordenada y1: \t");
	scanf("%f",&x);
	p1.y = x;
	printf("Coordenada z1: \t");
	scanf("%f",&x);
	p1.z = x;
	printf("\nIngrese las coordenadas del Segundo punto:\n");
	printf("Coordenada x2: \t");
	scanf("%f",&x);
	p2.x = x;
	printf("Coordenada y2: \t");
	scanf("%f",&x);
	p2.y = x;
	printf("Coordenada z2: \t");
	scanf("%f",&x);
	p2.z = x;
	x = euclideanDistance(p1,p2);
	printf("\nLa distancia es:\t%f",x);
	return 0;
}
